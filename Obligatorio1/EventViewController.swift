//
//  EventViewController.swift
//  Obligatorio1
//
//  Created by SP28 on 2/5/18.
//  Copyright © 2018 Ricardo Umpierrez. All rights reserved.
//

import UIKit

class EventViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var match:Match!
    @IBAction func localButton(_ sender: Any) {
        print("======================LOCAL=============================")
        self.performSegue(withIdentifier: "TeamInformation", sender: "home")
    }
    @IBAction func awayButton(_ sender: Any) {
        self.performSegue(withIdentifier: "TeamInformation", sender: "away")
    }
    @IBOutlet weak var localName: UILabel!
    @IBOutlet weak var awayName: UILabel!
    @IBOutlet weak var awayButton: UIButton!
    @IBOutlet weak var localButton: UIButton!
    @IBOutlet weak var stadiumImage: UIImageView!
    @IBOutlet weak var stadiumName: UILabel!
    @IBOutlet weak var date: UILabel!
    var allMatchEvents:[MatchEvent] = []
     @IBOutlet weak var tableView: UITableView!


    override func viewDidLoad() {
        super.viewDidLoad()
        localName?.text = match.homeTeam.name
        awayName?.text = match.awayTeam.name
        var localImage: String?
        localImage = match.homeTeam.logo
        ImagesLoad.setImageButton(url: URL(string: localImage!)! ,button: localButton)
        var awayImage: String?
        awayImage = match.awayTeam.logo
        ImagesLoad.setImageButton(url: URL(string: awayImage!)!, button: awayButton)
        date.text = match.date
        stadiumName.text = match.stadium.name
        var stadium_image: String?
        stadium_image = match.stadium.photo
        ImagesLoad.downloadImage(url: URL(string: stadium_image!)!, image: stadiumImage)
        for event in match.awayTeamEvents{
            allMatchEvents.append(event)
        }
        for event in match.homeTeamEvents{
            allMatchEvents.append(event)
        }
        allMatchEvents = allMatchEvents.sorted(by: { $0.time < $1.time })
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
            if sender as! String == "away" {
                if let destination = segue.destination as? TeamInformationViewController {
                    destination.squad = self.match.awayTeam
                }
            }
            else {
                if let destination = segue.destination as? TeamInformationViewController {
                    destination.squad = self.match.homeTeam
                }
            }
        }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if sender is String{
            return true
        }
        else{
            return false
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Here we set the quantity of cells to be created
        return match.awayTeamEvents.count + match.homeTeamEvents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Here we create the cells with information of the match
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventId", for: indexPath) as! EventCellViewController
           let results =  match.homeTeamEvents.filter { el in el === allMatchEvents[indexPath.item] }
            if results.count > 0 {
                cell.homeEventLabel.text =  eventTypeToEmoji(type: allMatchEvents[indexPath.item].typeOfEvent) + allMatchEvents[indexPath.item].player.name
                cell.awayEventLabel.text = ""
            } else {
                cell.awayEventLabel.text =  allMatchEvents[indexPath.item].player.name + eventTypeToEmoji(type: allMatchEvents[indexPath.item].typeOfEvent)
                cell.homeEventLabel.text = ""
            }
        
        cell.timeLabel.text = String( allMatchEvents[indexPath.item].time) + "'";
        return cell
    }
    
    func eventTypeToEmoji(type:MatchEvent.eventType) -> String{
        switch type {
        case MatchEvent.eventType.RED_CARD:
            return "🛑";
        case MatchEvent.eventType.YELLOW_CARD:
            return "🌕"
        case MatchEvent.eventType.GOAL:
            return "⚽️"
        case MatchEvent.eventType.SUBSTITUTION:
            return "💨"
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
