//
//  BoostrapData.swift
//  Obligatorio1
//
//  Created by Ricardo Umpierrez on 5/2/18.
//  Copyright © 2018 Ricardo Umpierrez. All rights reserved.
//

import UIKit

class BoostrapData: UIViewController {
    
    
    


    static func loadData() -> [Squad]{
        var uruguayPlayers = ["M. Campaña","F. Muslera","M. Silva","S. Coates","J. Giménez","D. Godín","M. Pereira","G. Silva","G. Varela","R. Bentancur","G. De Arrascaeta","D. Laxalt","N. Nández","G. Ramírez", "C. Rodríguez","C. Sánchez","L. Torreira","M. Vecino","E. Cavani","M. Gómez","C. Stuani","L. Suárez"];
        
        var brasilPlayers = ["Alisson","Neto","Ederson Moraes","Dani Alves","Marquinhos","Fágner","Rodrigo Caio","Thiago Silva","Pedro Geromel","Ismaily","João Miranda","Marcelo","Taison","Paulinho","Willian","Casemiro","Douglas Costa","Philippe Coutinho","Fred","Fernandinho","Renato Augusto","Talisca","Roberto Firmino","Willian José","Gabriel Jesus"];
        
        var rusiaPlayers = ["I. Akinfeev","V. Gabulov","A. Lunev","A. Selikhov","V. Granat","F. Kudryashov","I. Kutepov","R. Neustädter","K. Rausch","A. Semenov","I. Smolnikov","D. Cheryshev","A. Dzagoev","A. Erokhin","D. Glushakov","A. Golovin","V. Ignatyev","D. Kombarov","D. Kuzyaev","Aleksey Miranchuk","Anton Miranchuk","A. Samedov","A. Shvets","Y. Zhirkov","R. Zobnin","A. Kokorin","F. Smolov","A. Zabolotnyi"];
        
        var squadNames = [ "Uruguay", "Rusia", "Brasil", "Egipto", "Arabia Saudí", "Suiza", "Korea", "Narnia"];
        
        var squadDts = ["O.Tabarez", "S. Cherchesov", "Tite", "Héctor Cúper", "Juan Antonio Pizzi", "Vladimir Petković", "‎Shin Tae-yong", "Lion" ]
        
        var squadLogo = ["https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/062010/URU.png?itok=eAAQREyh", "https://i.pinimg.com/originals/69/8d/c1/698dc15138027560638ecef78867b4fd.png", "https://i.pinimg.com/originals/fa/c4/e6/fac4e64a21b800338ece2205a5cec1aa.png", "https://albawabacdn-albawabamiddleea.netdna-ssl.com/sites/default/files/imagecache/article_headline_node_big//sites/default/files/im/Sport/egyptian_fa__.jpg", "https://upload.wikimedia.org/wikipedia/fa/0/02/KSA-Badge.png", "https://i.pinimg.com/originals/62/c1/8c/62c18c1233d8e7d7e5535ea6b7acc762.jpg", "https://ih0.redbubble.net/image.288151759.1620/flat,800x800,075,f.u1.jpg" , "https://vignette.wikia.nocookie.net/animal-jam-clans-1/images/8/8f/Aslan.png/revision/20160927232819"]
        var squadPlayerNames = [uruguayPlayers, rusiaPlayers, brasilPlayers, [], [], [], [], [], [], [], [], []]
        
        var squadInstances = [Squad]();
        
        
        for index in 0...squadNames.count - 1  {
            var squad =  Squad(name : squadNames[index],coach: squadDts[index], players: [], matches: [], logo: squadLogo[index]);
            var indexPlayersCount = squadPlayerNames[index].count - 1;
            if(indexPlayersCount > 0){
                for indexPlayers in 0...indexPlayersCount{
                    var player =  Player(name:squadPlayerNames[index][indexPlayers], squad:squad, clubName: generateRandomSquadName());
                    squad.players.append(player);
                }
            }
            squadInstances.append(squad);
                
        }
        
        
        
        var squadStadiumMatchesRusia = ["Samara Arena", "Cosmos Stadium", "Nizhny Nogorov"];
        var squadStadiumMatchesPhoto = ["https://upload.wikimedia.org/wikipedia/commons/8/87/Samara_Arena1.jpg", "https://upload.wikimedia.org/wikipedia/commons/8/87/Samara_Arena1.jpg", "https://images.performgroup.com/di/library/GOAL/c3/de/nizhny-novgorod-stadium_1rz20c9v1ym7u1xgavp3zeqfhr.jpg?t=221867584"];
        var stadiumObjects = [Stadium]();
        for i in 0...squadStadiumMatchesRusia.count - 1{
            var stadium =  Stadium(name: squadStadiumMatchesRusia[i], photo: squadStadiumMatchesPhoto[i]);
            stadiumObjects.append(stadium);
        }
        
        
      
        
        var squadDateMatchesRusia = ["Jue 14/06/18 12:00", "Mar 19/06/18 15:00", "Lun 25/06/18 11:00"];
        var squadCountryMatchesRusia = [[squadInstances[1], squadInstances[4]],[squadInstances[3], squadInstances[1]], [squadInstances[1], squadInstances[0]]];
        
        
        var squadDateCountryMatchesUruguay = ["Vie  15/06/18 09 : 00", "Mie   20/06/18 12 : 00"]
        var squadCountryMatchesUruguay = [[squadInstances[3], squadInstances[0]], [squadInstances[0] , squadInstances[5] ]]
        
        var squadDateCountryMatchesBrasil = ["Dom   17/06/18 15 : 00", "Vie    22/06/18 09 : 00", "Mie  27/06/18 15 : 00"]
        var squadCountryMatchesBrasil = [[squadInstances[5], squadInstances[2]], [squadInstances[6], squadInstances[2]],  [squadInstances[7], squadInstances[2]]];
        
        var allSquadDateMatches = squadDateMatchesRusia + squadDateCountryMatchesUruguay + squadDateCountryMatchesBrasil
        var allSquadCountryMatches = squadCountryMatchesRusia + squadCountryMatchesUruguay + squadCountryMatchesBrasil;
        
        var allSquadStadiums = stadiumObjects + stadiumObjects + stadiumObjects
        
        
        for i in 0...allSquadDateMatches.count - 1{
            var match =  Match(stadium: allSquadStadiums[i], date: allSquadDateMatches[i], match_phase:"Grupo A", homeTeam: allSquadCountryMatches[i][0], awayTeam:allSquadCountryMatches[i][1], homeTeam_event: generateRandomEvenets(squad: allSquadCountryMatches[i][0]) , awayTeamEvents: generateRandomEvenets(squad: allSquadCountryMatches[i][1]))
            match.homeTeam.matches.append(match);
            match.awayTeam.matches.append(match);
        }
        
        return squadInstances;
        
    }
    
    static func  generateRandomEvenets(squad: Squad) -> [MatchEvent]{
        var eventObjects = [MatchEvent]();
        for i in  0...3{
            var randomNum = randomNumber(range: 1...90);
             var randomEventType = randomNumber(range: 0...3);
            var squadPLayersCount = squad.players.count - 1;
            var hasPlayers = squadPLayersCount > 0;
            if(hasPlayers){
                var randomPlayer = randomNumber(range: 0...squadPLayersCount);
                var eventTypeOptions = [MatchEvent.eventType.GOAL, MatchEvent.eventType.RED_CARD, MatchEvent.eventType.SUBSTITUTION, MatchEvent.eventType.YELLOW_CARD]
                var event = MatchEvent(type: eventTypeOptions[randomEventType], player: squad.players[randomPlayer], time:randomNum)
                eventObjects.append(event);
            }
        }
        return eventObjects;
    }
    
    static func randomNumber(range: ClosedRange<Int> = 1...6) -> Int {
        let min = range.lowerBound
        let max = range.upperBound
        return Int(arc4random_uniform(UInt32(1 + max - min))) + min
    }
    
     static func  generateRandomSquadName() -> String{
        let squadNames =   ["C. A. Juventud","Plaza Colonia","Cerro Largo F.C.","C.A. Rentistas","Tacuarembó F.C.","C.A. Villa Teresa","I. A. Sud América","Deportivo Maldonado","C.S. Cerrito","C. Oriental F.","C.S. y D. Villa Española","Albion F.C.","C.S. Miramar Misiones","Central Español F.C"]
        
        return squadNames[randomNumber(range: 0...squadNames.count - 1)]
    }


    



}
