
import Foundation

public class MatchEvent {
    
    public enum eventType
    {
        case RED_CARD, YELLOW_CARD, GOAL, SUBSTITUTION
    
    }
	public var typeOfEvent : eventType
	public var player : Player
	public var time : Int
    
    public init(type: eventType, player: Player, time: Int){
        self.typeOfEvent = type;
        self.player = player;
        self.time = time;
    }

}
