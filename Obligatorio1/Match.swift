

import Foundation
 

public class Match {
	public var stadium : Stadium
	public var date : String
    public var match_phase : String
	public var homeTeam : Squad
	public var awayTeam : Squad
    public var goalsHome: Int?
    public var goalsAway: Int?
    public var homeTeamEvents : [MatchEvent] = []
    public var awayTeamEvents : [MatchEvent] = []
    
    
    init(stadium:Stadium, date:String, match_phase:String, homeTeam:Squad, awayTeam:Squad, homeTeam_event:[MatchEvent] , awayTeamEvents:[MatchEvent]) {
        self.stadium = stadium;
        self.date = date;
        self.homeTeam=homeTeam;
        self.awayTeam=awayTeam;
        self.homeTeamEvents = homeTeam_event;
        self.awayTeamEvents = awayTeamEvents;
        self.match_phase = match_phase;
    }
    
    init(stadium:Stadium, date:String, match_phase:String, homeTeam:Squad, awayTeam:Squad, homeTeam_event:[MatchEvent] , awayTeamEvents:[MatchEvent], goalsHome: Int, goalsAway: Int) {
        self.stadium = stadium;
        self.date = date;
        self.homeTeam=homeTeam;
        self.awayTeam=awayTeam;
        self.homeTeamEvents = homeTeam_event;
        self.awayTeamEvents = awayTeamEvents;
        self.match_phase = match_phase;
        self.goalsHome = goalsHome;
        self.goalsAway = goalsAway;
    }

}
