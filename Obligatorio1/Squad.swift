

import Foundation

public class Squad {
	public var name : String?
	public var coach : String?
	public var players : [Player] = []
	public var matches : [Match] = []
    public var logo: String?
    
    
    public init(name: String, coach: String, players: [Player], matches: [Match], logo: String){
        self.name = name;
        self.coach = coach;
        self.players = players;
        self.matches = matches;
        self.logo = logo;
    }

}





