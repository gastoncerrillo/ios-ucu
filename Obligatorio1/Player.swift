
import Foundation

public class Player {
	public var name : String
	public var squad : Squad
    public var clubName: String

    init(name: String, squad: Squad, clubName: String){
    self.name = name;
    self.squad = squad;
    self.clubName = clubName
}
}
