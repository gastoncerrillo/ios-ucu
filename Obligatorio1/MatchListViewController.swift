//
//  MatchListTable.swift
//  Obligatorio1
//
//  Created by SP28 on 26/4/18.
//  Copyright © 2018 Ricardo Umpierrez. All rights reserved.
//

import Foundation
import UIKit

class MatchListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tablaLabel: UILabel!
    @IBOutlet weak var collectionLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    var squads:[Squad] = [];
    var matches:[Match] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.reloadData()
        tableView.dataSource = self

        squads = BoostrapData.loadData();
        
        for squad in squads{
            for match in squad.matches{
                matches.append(match)
            }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow{
            let selectedRow = indexPath.row
            let controller = segue.destination as! EventViewController
            controller.match = matches[selectedRow]
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Here we set the quantity of cells to be created
        return matches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Here we create the cells with information of the match
        let cell = tableView.dequeueReusableCell(withIdentifier: "matchId", for: indexPath) as! MatchCellViewController
        cell.awayPhoto.image = nil
        cell.homePhoto.image = nil
        cell.homeLabel.text = matches[indexPath.item].awayTeam.name
        cell.awayLabel.text = matches[indexPath.item].homeTeam.name
        cell.matchPhase.text = matches[indexPath.item].match_phase
        var localImage: String!
        localImage = matches[indexPath.item].homeTeam.logo
        ImagesLoad.downloadImage(url: URL(string: localImage!)! ,image: cell.awayPhoto)
        
        var awayImage: String!
        awayImage = matches[indexPath.item].awayTeam.logo
        ImagesLoad.downloadImage(url: URL(string: awayImage!)! ,image: cell.homePhoto)
        cell.date.text = matches[indexPath.item].date
        cell.stadiumName.text = matches[indexPath.item].stadium.name
        return cell
        }
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
     }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

