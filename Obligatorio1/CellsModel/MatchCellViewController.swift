import UIKit

class MatchCellViewController: UITableViewCell {
    
    
    required override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    @IBOutlet weak var matchPhase: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var awayLabel: UILabel!
    @IBOutlet weak var homePhoto: UIImageView!
    @IBOutlet weak var awayPhoto: UIImageView!
    @IBOutlet weak var stadiumName: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
}

