//
//  NextMatchCollectionViewCell.swift
//  Obligatorio1
//
//  Created by SP28 on 8/5/18.
//  Copyright © 2018 Ricardo Umpierrez. All rights reserved.
//

import UIKit

class NextMatchCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var squadName: UILabel!
    
    @IBOutlet weak var stadiumName: UILabel!
    @IBOutlet weak var matchDate: UILabel!
    @IBOutlet weak var squadLogo: UIImageView!
}
