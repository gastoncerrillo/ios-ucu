import UIKit

class PlayerCellViewController: UITableViewCell {
    
    
    required override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    @IBOutlet weak var playerNumber: UILabel!
    @IBOutlet weak var squadName: UILabel!
    @IBOutlet weak var playerName: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
}


