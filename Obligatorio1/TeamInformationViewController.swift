//
//  TeamInformationViewController.swift
//  Obligatorio1
//
//  Created by Ricardo Umpierrez on 5/3/18.
//  Copyright © 2018 Ricardo Umpierrez. All rights reserved.
//

import UIKit

class TeamInformationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,  UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var dtName: UILabel!
    @IBOutlet weak var dtNumber: UILabel!
    var squad:Squad?
    var playerQuantity:Int?
    var logoUrl:String?
    @IBOutlet weak var teamIcon: UIImageView!
    @IBOutlet weak var playerListTable: UITableView!
    @IBOutlet weak var SquadImage: UIImageView!
    @IBOutlet weak var nextMatchCollectionView: UICollectionView!
    var items = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        playerListTable.delegate = self
        playerListTable.dataSource = self
        playerListTable.reloadData()
        
        nextMatchCollectionView.delegate = self
        nextMatchCollectionView.dataSource = self
        nextMatchCollectionView.reloadData()
        
        if let squad = squad{
            title = squad.name
            playerQuantity = squad.players.count
            logoUrl = squad.logo
            ImagesLoad.downloadImage(url: URL(string: logoUrl!)!, image: teamIcon)
            dtName.text = squad.coach;
            dtNumber.text = "DT";
        }
       
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Here we set the quantity of cells to be created
        if let playerQuantity = playerQuantity{
            return playerQuantity
        }
        else{
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Here we create the cells with information of the match
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "playerId", for: indexPath) as! PlayerCellViewController
        cell.playerName.text = squad!.players[indexPath.item].name
        cell.playerNumber.text = String(indexPath.item)
        //cell.tea.text = squad?.players[indexPath.item].name
        cell.squadName.text = squad!.players[indexPath.item].clubName


        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.squad?.matches.count)!
    }
    
    // make a cell for each cell index path
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UICollectionViewCell,
            let indexPath = nextMatchCollectionView.indexPath(for: cell) {
            
            let vc = segue.destination as! EventViewController //Cast with your DestinationController
            //Now simply set the title property of vc
            vc.match = squad?.matches[indexPath.row]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nextMatch", for: indexPath as IndexPath) as! NextMatchCollectionViewCell
        if(self.squad?.matches[indexPath.item].awayTeam.name != squad?.name){
            cell.squadName.text = self.squad?.matches[indexPath.item].awayTeam.name
            var squad_image: String?
            squad_image = self.squad?.matches[indexPath.item].awayTeam.logo
            ImagesLoad.downloadImage(url: URL(string: squad_image!)!, image: cell.squadLogo)
        }
        else{
            cell.squadName.text = self.squad?.matches[indexPath.item].homeTeam.name
            var squad_image: String?
            squad_image = self.squad?.matches[indexPath.item].homeTeam.logo
            ImagesLoad.downloadImage(url: URL(string: squad_image!)!, image: cell.squadLogo)
        }
        cell.matchDate.text = self.squad?.matches[indexPath.item].date
        cell.stadiumName.text = self.squad?.matches[indexPath.item].stadium.name
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 10
        return cell
    }
    
    
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

