

import Foundation
 

public class Stadium {
	public var name : String
	public var photo : String
    
    public init(name: String, photo: String){
        self.name = name;
        self.photo = photo;
    }
}
